@extends("layouts.app")
@section("content")
<div class="container">
    <div class="col-md-12">
        <div class="row vacancy">
            <h2 class="text-center">
                <?= trans('vacancyHandler.header'); ?>
            </h2>
            <!-- Display errors -->
            @include("errors.validation")
            <!-- Vacancy handler -->
            <form action="?" method="POST" class="form">
                {{ csrf_field() }}
                <table class="table">
                    <tr>
                        <th>
                            <?= trans('vacancyHandler.title'); ?>
                        </th>
                        <td>
                            <input class="form-control" type="text" name="title">
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?= trans('vacancyHandler.employer'); ?>
                        </th>
                        <td>
                            <input class="form-control" type="text" name="employer">
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?= trans('vacancyHandler.description'); ?>
                        </th>
                        <td>
                            <textarea class="form-control" name="description"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>
                            <button class="btn btn-primary save">
                                <?= trans('vacancyHandler.add'); ?>
                            </button>
                            <button type="reset" class="btn btn-default">
                                <?= trans('vacancyHandler.reset'); ?>
                            </button>
                            
                            
                            <?php foreach (Config::get('app.locales') as $locale => $language): ?>
                                <a class="btn btn-circle pull-right" href='<?= url($locale, "add_vacancy") ?>'>{{$locale}}</a>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
@endsection