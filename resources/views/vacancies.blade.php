@extends("layouts.app")
@section("content")
    <div class="container">
        <h2 class="text-center">
            Available vacancies
        </h2>
    </div>
    @if (count($vacancies) > 0)
        @foreach ($vacancies as $vacancy)
            <div class="container">
                <div class="col-md-12">
                    <div class="row vacancy">
                        <table class="table">
                            <tr>
                                <th>Vacancy:</th>
                                <td>{{ $vacancy->title }}</td>
                            </tr>
                            <tr>
                                <th>Employer:</th>
                                <td>{{ $vacancy->employer }}</td>
                            </tr>
                            <tr>
                                <th>Date:</th>
                                <td>{{ $vacancy->created_at }}</td>
                            </tr>
                            <tr>
                                <th>Description:</th>
                                <td>{{ $vacancy->description }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
    
    <div class="container">
        <div class="alert">
            There's {{count($vacancies)}} available vacancies
        </div>
    </div>   
    
@endsection