<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Test task</title>
        <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/css/common.css">
    </head>
    <body>
        <nav class="container-fluid">
            <ul>
                <li><a href="<?= route('index');?>">Main</a></li>
                <li><a href="<?= route('vacancies');?>">Vacancies</a></li>
                <li><a href="<?= route('add_vacancy');?>">Add vacancy</a></li>
            </ul>
        </nav>
        @yield("content")
        <footer class="container">
            <div class="col-md-12">
                <div class="row">
                    Copyleft <s>&copy;</s> | Test task
                </div>
            </div>
        </footer>
        <!--
            Script's initialization
        -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/main.js"></script>
    </body>
</html>
