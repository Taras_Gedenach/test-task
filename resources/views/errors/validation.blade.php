@if (count($errors) > 0)
    <!-- Validation errors -->
    <div class="alert alert-danger">
        Errors while parsing form
    </div>
    @foreach ($errors->all() as $error)
        <div class="alert alert-danger">
            {{ $error }}
        </div>
    @endforeach
@endif