<?php
return [
    'header' => 'Добавить вакансию',
    'title' => 'Вакансия:',
    'employer' => 'Роботодатель:',
    'description' => 'Описание:',
    'add' => 'Добавить',
    'reset' => 'Очистить'
];