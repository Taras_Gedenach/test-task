<?php
return [
    'header' => 'Add vacancy',
    'title' => 'Vacancy:',
    'employer' => 'Employer:',
    'description' => 'Description:',
    'add' => 'ADD',
    'reset' => 'RESET',
];