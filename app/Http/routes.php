<?php
use App\Vacancy;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
$router->get('locale', ['as' => 'locale', 'uses' => 'HomeController@locale']);
$router->get('/', ['as' => 'index', 'uses' => 'HomeController@index']);


Route::get('/', array('as' => 'index', function () {
    return view('welcome');
}));
/*
    Display all vacancies
*/
Route::get('/vacancies', array('as' => 'vacancies', function () {
    $vacancies = Vacancy::orderBy('created_at', 'asc')->get();

    return view('vacancies', [
        'vacancies' => $vacancies
    ]);
}));

/*
    Add vacancy
*/
Route::get('/add_vacancy', array('as' => 'add_vacancy', function () {
    return view('vacancyHandler');
}));

/*
    Parse input data from vacancy handler
*/
Route::post('/add_vacancy', array('as' => 'add_vacancy', function(Request $request) {
    $validator = Validator::make($request->all(), [
        'employer' => 'required|max:255',
        'title' => 'required|max:255',
        'description' => 'required'
    ]);
    
    if ($validator->fails()) {
        return redirect('add_vacancy')
            ->withInput()
            ->withErrors($validator);
    }
    
    $vacancy = new Vacancy();
    $vacancy->title = $request->title;
    $vacancy->employer = $request->employer;
    $vacancy->description = $request->description;
    $vacancy->save();
    
    return redirect('/vacancies');
}));