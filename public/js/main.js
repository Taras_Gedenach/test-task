/*jshint browser: true, jquery: true*/

$(document).ready(function() {
    'use strict';
    
    //Proceed validation for errors
    function validate(input) {
        var errors = 0;
        //Remove all warnings
        $(".vacancy form .alert").remove();
        
        $.each(input, function(key, elem) {
            if ($(elem).val().trim() === "") {
                $(elem).before(
                    "<div class='alert alert-danger'>This field is required</div>"
                );
                errors++;
            }
        });
        
        if (errors > 0) {
            return false;
        } else {
            return true;
        }
    }
    
    $(".save").click(function(event){
        //Collect all input elements
        var input = $(this).parents("form").find("input, textarea");
        
        //Prevent dafault behaviour on button click
        event.preventDefault();
        
        //Validate form
        if (validate(input)) {
            
            $(this).parents("form").submit();
        }
    });
    
    //When value of input element changes, remove it's warning
    $("form input, form textarea").change(function() {
        $(this).siblings(".alert").remove();
    });
});